# ECE651-Server

## Useful Links:

1. [Run Containers](https://docs.docker.com/language/nodejs/run-containers/)
2. [Example Flask Server - 1](https://towardsdatascience.com/the-right-way-to-build-an-api-with-python-cd08ab285f8f)
3. [Example Flask Server - 2](https://github.com/MoranLi/pythonBook/blob/master/server.py)
4. [DB Diagram](https://lucid.app/lucidchart/35624447-89f0-4d49-970e-56923ad0cfa4/edit?viewport_loc=-11%2C-119%2C2219%2C1065%2C0_0&invitationId=inv_525e69db-2a2b-4bb1-88ca-7030b1b39586)

## Run Unit Tests:

```sh
    # Under ece651-server folder
    python3 -m pytest -v
```

> Could use the ```pre-push``` hook, see details on main project.

## To Debug:

```sh
    docker run -it <image_hash> bash
```