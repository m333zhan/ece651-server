CREATE DATABASE studentcare;

USE studentcare;

CREATE TABLE Users (
    user_name VARCHAR(255) NOT NULL PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    student_id VARCHAR(255),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    sex VARCHAR(255),
    birthday VARCHAR(255)
);

CREATE TABLE AccessTokens (
    user_name VARCHAR(255) NOT NULL PRIMARY KEY,
    token VARCHAR(255) NOT NULL,
    expires_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    FOREIGN KEY (user_name) REFERENCES Users (user_name)
);

CREATE TABLE ClaimHistory (
    claim_id INT AUTO_INCREMENT NOT NULL,
    user_name VARCHAR(255) NOT NULL,
    creation_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    amount DECIMAL(15, 2) NOT NULL,
    type VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL,
    comment TEXT NOT NULL,
    add_on TEXT NOT NULL,
    PRIMARY KEY (claim_id),
    FOREIGN KEY (user_name) REFERENCES Users (user_name)
);

-- Test Account Info
INSERT INTO
    Users (user_name, password, email)
VALUES
    ('test', 'test', 'test@test.com');

INSERT INTO
    ClaimHistory (
        user_name,
        amount,
        type,
        status,
        comment,
        add_on
    )
VALUES
    (
        'test',
        350.22,
        'Dental',
        'Processing',
        'Test comment 1',
        'Test Add on 1'
    ),
    (
        'test',
        5310.3,
        'Massage',
        'Finished',
        'Test comment 2',
        'Test Add on 2'
    );

-- Remote access account
CREATE USER 'studentcare'@'localhost' IDENTIFIED BY 'secret';
CREATE USER 'studentcare'@'%' IDENTIFIED BY 'secret';

GRANT ALL PRIVILEGES ON *.* TO 'studentcare'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'studentcare'@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;