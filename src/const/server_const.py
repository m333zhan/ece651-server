import os


class MiscConst(object):
    # Store server const
    expires_in = 3600

    db_connect_host_env = os.environ['SERVER_DB_CONNECTION_HOST'] if 'SERVER_DB_CONNECTION_HOST' in os.environ else 'localhost'
    db_connect_user_name_env = os.environ['DB_USER_NAME'] if 'DB_USER_NAME' in os.environ else 'root'
    db_connect_password_env = os.environ['MYSQL_ROOT_PASSWORD'] if 'MYSQL_ROOT_PASSWORD' in os.environ else 'secret'
    db_connect_port_env = os.environ['SERVER_DB_CONNECTION_PORT'] if 'SERVER_DB_CONNECTION_PORT' in os.environ else '30306'
    db_connect_db_name_env = os.environ['DB_NAME'] if 'DB_NAME' in os.environ else 'studentcare'

    general_success_code = 200
    general_client_side_error_code = 400
    client_side_auth_failure_error_code = 401
    claim_amount_unrecognized_error_code = 402
    general_server_side_error_code = 500

    authorization_failed = "Authorization failed!"
    unsupported_content_type = 'Unsupported Content-Type'


class RegisterAPIConst(object):
    # API message - Register
    token_not_available = 'Token not available for this user.'
    missing_fields = 'Missing fields.'
    user_exist = 'An entry with the user_name exists.'


class AuthAPIConst(object):
    # API message - Auth
    wrong_user_name_or_password = 'Invalid username or password.'
    missing_fields = 'Missing fields.'
    user_exist = 'An entry with the user_name exists.'


class UserAPIConst(object):
    # API message - User
    missing_fields = 'Missing fields.'
    unauthorized_access = "Unauthorized Access."
    user_does_not_exist = 'An entry with the user_name does not exist.'
    token_expired = "Unauthorized - Token expired."


class ClaimAPIConst(object):
    # API message - Claim
    insertion_failed = 'History insertion failed!'
    missing_fields = 'Missing fields'
    invalid_image = 'Invalid Image Uploaded'
    amount_unrecognized = 'No Number Recognized in Claim Uploaded'


class TextRecogConst(object):
    # Percentage of sharpness for shaded and unshaded pictures
    Shaded_sharpness = 90
    Unshaded_sharpness = 40

    # Thresholds for Hough lines
    Hough_thre_line = 250
    Hough_thre_line_text = 200

    # Edge detection thresholds
    T_lower = 50
    T_upper = 150

    # Minimum hough angle difference
    min_rad = 1e-3
