from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
from mysql.connector import connect, Error
from mysql.connector.locales.eng import client_error
from functools import wraps
from src.const import server_const as ServerConst
from src.text_recog.text_recog import TextRecog
from PIL import Image
from io import BytesIO
import os
import jwt
import datetime
import logging
import base64

app = Flask(__name__)
CORS(app, supports_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'
api = Api(app)

# Configure logger
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)


def token_required(f):
    # Decorator function for checking Authorization header
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            if request.headers['Authorization'].startswith('Bearer '):
                token = request.headers['Authorization'][7:]

        if not token:
            return {"error": ServerConst.MiscConst.client_side_auth_failure_error_code,
                    "message": ServerConst.MiscConst.authorization_failed}, \
                ServerConst.MiscConst.client_side_auth_failure_error_code

        return f(*args, **kwargs)

    return decorated


def form_data_required(f):
    # Decorator function for checking Content-Type header to be form data
    @wraps(f)
    def decorated(*args, **kwargs):
        content_type = request.headers.get('Content-Type')

        if 'application/x-www-form-urlencoded' not in content_type and 'multipart/form-data' not in content_type:
            return {"error": ServerConst.MiscConst.general_client_side_error_code,
                    "message": ServerConst.MiscConst.unsupported_content_type}, \
                ServerConst.MiscConst.general_client_side_error_code

        return f(*args, **kwargs)

    return decorated


def api_check_token_payload_match(f):
    # Decorator function for checking whether the token payload matches the provided info
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            user_data = None
            content_type = request.headers.get('Content-Type')

            if 'application/json' in content_type:
                # Request contains JSON data
                user_data = request.json
            elif 'application/x-www-form-urlencoded' in content_type or 'multipart/form-data' in content_type:
                # Request contains form data
                user_data = request.form
            else:
                # Unsupported Content-Type
                return {"error": ServerConst.MiscConst.general_client_side_error_code,
                        "message": ServerConst.MiscConst.unsupported_content_type}, \
                    ServerConst.MiscConst.general_client_side_error_code

            # Check missing field
            if "user_name" not in user_data:
                return {"error": ServerConst.MiscConst.general_client_side_error_code,
                        "message": ServerConst.UserAPIConst.missing_fields}, \
                    ServerConst.MiscConst.general_client_side_error_code

            user_name = user_data["user_name"]
            token = request.headers['Authorization'][7:]
            decoded_payload = decode_token(token)

            # If the decoded user_name does not match the request body
            if decoded_payload["user_name"] != user_name:
                return {"error": ServerConst.MiscConst.general_client_side_error_code,
                        "message": ServerConst.UserAPIConst.unauthorized_access}, \
                    ServerConst.MiscConst.general_client_side_error_code

            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Check whether the user_name exists
                    check_exist_query = "SELECT COUNT(*) FROM Users WHERE user_name = %s"
                    # To create a tuple with a single value, you need to add a comma after the value.
                    values = (user_name,)
                    cursor.execute(check_exist_query, values)
                    result = cursor.fetchone()
                    if result[0] == 0:
                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.UserAPIConst.user_does_not_exist}, \
                            ServerConst.MiscConst.general_client_side_error_code

                    # Check whether the token exists
                    check_token_query = "SELECT COUNT(*) FROM AccessTokens WHERE user_name = %s AND token = %s"
                    # To create a tuple with a single value, you need to add a comma after the value.
                    values = (user_name, token)
                    cursor.execute(check_token_query, values)
                    result = cursor.fetchone()
                    if result[0] == 0:
                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.UserAPIConst.token_expired}, \
                            ServerConst.MiscConst.general_client_side_error_code

                    # Check if the token expires
                    time_diff = datetime.datetime.utcnow() - \
                        datetime.datetime.fromtimestamp(
                            decoded_payload["expires_at"])
                    if time_diff.total_seconds() > ServerConst.MiscConst.expires_in:
                        # Delete access_token if expired
                        delete_token_query = ("DELETE FROM AccessTokens "
                                              "WHERE user_name = %s AND token = %s")
                        values = (decoded_payload["user_name"], token)
                        cursor.execute(
                            delete_token_query, values)

                        # Commit the transaction
                        cnx.commit()

                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.UserAPIConst.unauthorized_access}, \
                            ServerConst.MiscConst.general_client_side_error_code
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code

        return f(*args, **kwargs)

    return decorated


def connect_db():
    # Create db connection
    """
    for local connection (dev/testing), use the following fields:
    host='localhost',
    user='root',
    password='secret',
    port=30306,
    database='studentcare'
    """
    try:
        return connect(
            host=ServerConst.MiscConst.db_connect_host_env,
            user=ServerConst.MiscConst.db_connect_user_name_env,
            password=ServerConst.MiscConst.db_connect_password_env,
            port=ServerConst.MiscConst.db_connect_port_env,
            database=ServerConst.MiscConst.db_connect_db_name_env
        )
    except Error as e:
        logger.error(e)
        raise e


def generate_token(user_name):
    secret = ServerConst.MiscConst.db_connect_password_env
    # Token will expire in one hour
    expiration = datetime.datetime.utcnow() + datetime.timedelta(hours=1)
    # Remove high precision info
    expiration = expiration.replace(second=0, microsecond=0)
    payload = {"user_name": user_name,
               "expires_at": int(expiration.timestamp())}
    token = jwt.encode(payload, secret, algorithm="HS256")
    return token, expiration


def decode_token(token):
    secret = ServerConst.MiscConst.db_connect_password_env
    return jwt.decode(token, secret, algorithms=["HS256"])


class User(Resource):
    # User endpoint

    @cross_origin()
    @token_required
    @api_check_token_payload_match
    def post(self):
        # Get user profile
        try:
            user_data = request.get_json()
            user_name = user_data["user_name"]

            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Get user profile
                    get_user_profile_query = "SELECT * FROM Users WHERE user_name = %s"
                    values = (user_name,)
                    cursor.execute(get_user_profile_query, values)
                    result = cursor.fetchone()
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return ServerConst.MiscConst.general_server_side_error_code

        user_name, _, email, student_id, first_name, last_name, sex, birthday = result
        return {'user_name': user_name,
                'email': email,
                'student_id': student_id,
                'first_name': first_name,
                'last_name': last_name,
                'sex': sex,
                'birthday': birthday}, ServerConst.MiscConst.general_success_code

    @cross_origin()
    @token_required
    @api_check_token_payload_match
    def put(self):
        # Update user profile
        try:
            user_data = request.get_json()
            user_name = user_data["user_name"]
            password = None if 'password' not in user_data else user_data["password"]
            email = None if 'email' not in user_data else user_data["email"]
            student_id = None if 'student_id' not in user_data else user_data["student_id"]
            first_name = None if 'first_name' not in user_data else user_data["first_name"]
            last_name = None if 'last_name' not in user_data else user_data["last_name"]
            sex = None if 'sex' not in user_data else user_data["sex"]
            birthday = None if 'birthday' not in user_data else user_data["birthday"]

            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Update user profile
                    update_user_profile_query = (
                        "UPDATE Users "
                        "SET "
                        "password = IFNULL(%s, password), "
                        "email = IFNULL(%s, email), "
                        "student_id = IFNULL(%s, student_id), "
                        "first_name = IFNULL(%s, first_name), "
                        "last_name = IFNULL(%s, last_name), "
                        "sex = IFNULL(%s, sex), "
                        "birthday = IFNULL(%s, birthday) "
                        "WHERE user_name = %s"
                    )
                    values = (
                        password,
                        email,
                        student_id,
                        first_name,
                        last_name,
                        sex,
                        birthday,
                        user_name
                    )
                    cursor.execute(update_user_profile_query, values)
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code

        return {"code": ServerConst.MiscConst.general_success_code}, ServerConst.MiscConst.general_success_code


class Register(Resource):
    # Register endpoint
    # See swagger doc for user profile

    @cross_origin()
    def post(self):
        user_data = request.get_json()
        if "user_name" not in user_data or "password" not in user_data or "email" not in user_data:
            return {"error": ServerConst.MiscConst.general_client_side_error_code,
                    "message": ServerConst.RegisterAPIConst.missing_fields}, \
                ServerConst.MiscConst.general_client_side_error_code
        user_name = user_data["user_name"]
        password = user_data["password"]
        email = user_data["email"]

        try:
            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Check whether the user_name exists
                    check_exist_query = "SELECT COUNT(*) FROM Users WHERE user_name = %s"
                    # To create a tuple with a single value, you need to add a comma after the value.
                    values = (user_name,)
                    cursor.execute(check_exist_query, values)
                    result = cursor.fetchone()
                    if result[0] > 0:
                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.RegisterAPIConst.user_exist}, \
                            ServerConst.MiscConst.general_client_side_error_code
                    # Execute the create_user_query with the values
                    create_user_query = ("INSERT INTO Users "
                                         "(user_name, password, email) "
                                         "VALUES (%s, %s, %s)")
                    values = (user_name, password, email)
                    cursor.execute(create_user_query, values)
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code
        return {"code": ServerConst.MiscConst.general_success_code}, ServerConst.MiscConst.general_success_code


class Login(Resource):
    # Auth Logout endpoint

    @cross_origin()
    def post(self):
        user_data = request.get_json()
        if "user_name" not in user_data or "password" not in user_data:
            return {"error": ServerConst.MiscConst.general_client_side_error_code,
                    "message": ServerConst.AuthAPIConst.missing_fields}, \
                ServerConst.MiscConst.general_client_side_error_code
        user_name = user_data["user_name"]
        password = user_data["password"]

        try:
            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Check whether the user_name exists and if the password matches
                    check_exist_query = "SELECT password FROM Users WHERE user_name = %s"
                    # To create a tuple with a single value, you need to add a comma after the value.
                    values = (user_name,)
                    cursor.execute(check_exist_query, values)
                    result = cursor.fetchone()
                    if result is None or result[0] != password:
                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.AuthAPIConst.wrong_user_name_or_password}, \
                            ServerConst.MiscConst.general_client_side_error_code
                    # Add token into the table with duplicate update
                    token, exp = generate_token(user_name)
                    values = (user_name, token, exp)
                    create_token_query = ("INSERT INTO AccessTokens "
                                          "(user_name, token, expires_at) "
                                          "VALUES (%s, %s, %s) "
                                          "ON DUPLICATE KEY UPDATE token = VALUES(token), expires_at = VALUES(expires_at)")
                    cursor.execute(create_token_query, values)
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code

        return {'access_token': token, 'token_type': "Bearer",
                'expires_in': 3600}, ServerConst.MiscConst.general_success_code


class Logout(Resource):
    # Auth Logout endpoint

    @cross_origin()
    @token_required
    def post(self):
        response = {
            "code": ServerConst.MiscConst.general_success_code}, ServerConst.MiscConst.general_success_code

        try:
            token = request.headers['Authorization'][7:]
            decoded_payload = decode_token(token)
            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Delete access_token if exist
                    delete_and_check_exist_query = ("DELETE FROM AccessTokens "
                                                    "WHERE user_name = %s AND token = %s AND expires_at = %s")
                    values = (decoded_payload["user_name"],
                              token, datetime.datetime.fromtimestamp(decoded_payload["expires_at"]))
                    cursor.execute(
                        delete_and_check_exist_query, values)

                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return response

        return response


class Claim(Resource):
    # claim endpoint

    @cross_origin()
    @token_required
    @form_data_required
    @api_check_token_payload_match
    def post(self):
        try:
            claim_data = request.form

            if 'type' not in claim_data or 'comment' not in claim_data or \
                    'add_on' not in claim_data or 'image' not in claim_data:
                return {"error": ServerConst.MiscConst.general_client_side_error_code,
                        "message": ServerConst.ClaimAPIConst.missing_fields},\
                    ServerConst.MiscConst.general_client_side_error_code

            user_name = claim_data['user_name']
            type = claim_data['type']
            comment = claim_data['comment']
            add_on = claim_data['add_on']
            claim_pic_base64 = claim_data['image']

            recognizer = TextRecog()
            open_result = recognizer.open_image(claim_pic_base64)
            recog_result = recognizer.text_recog()

            if open_result == ServerConst.MiscConst.general_client_side_error_code:
                return {"error": ServerConst.MiscConst.general_client_side_error_code,
                        "message": ServerConst.ClaimAPIConst.invalid_image}, \
                    ServerConst.MiscConst.general_client_side_error_code

            if recog_result == ServerConst.MiscConst.claim_amount_unrecognized_error_code:
                return {"error": ServerConst.MiscConst.claim_amount_unrecognized_error_code,
                        "message": ServerConst.ClaimAPIConst.amount_unrecognized},\
                    ServerConst.MiscConst.claim_amount_unrecognized_error_code

            amount = recognizer.amount

            # if the result of recognize is different from uploaded
            if type is None or recognizer.category.lower() != type.lower():
                status = 'processing'
            else:
                status = 'finished'

            this_claim_id = None
            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Create a new claim
                    create_claim_query = (
                        'INSERT INTO ClaimHistory '
                        '(user_name, amount, type, status, comment, add_on) '
                        'VALUES (%s, %s, %s, %s, %s, %s)'
                    )
                    values = (user_name, amount, type,
                              status, comment, add_on)
                    cursor.execute(create_claim_query, values)

                    # Return the generated claim history id
                    get_claim_history_id = 'SELECT LAST_INSERT_ID()'
                    cursor.execute(get_claim_history_id)
                    result = cursor.fetchone()
                    if result is None:
                        return {"error": ServerConst.MiscConst.general_client_side_error_code,
                                "message": ServerConst.ClaimAPIConst.insertion_failed}, \
                            ServerConst.MiscConst.general_client_side_error_code

                    this_claim_id = result[0]
                    # Commit the transaction
                    cnx.commit()

            # Convert the base64 string to a PIL Image object
            img_data = base64.decodebytes(
                bytes(claim_pic_base64.split(',')[1], "utf-8"))
            img = Image.open(BytesIO(img_data))

            # Save the image as a PNG file
            # create any missing directories in the relative path
            relative_path = f"./claimHistory/{user_name}/{this_claim_id}.png"
            os.makedirs(os.path.dirname(relative_path), exist_ok=True)
            img.save(relative_path, "PNG")

        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code

        return {"code": ServerConst.MiscConst.general_success_code}, ServerConst.MiscConst.general_success_code


class ClaimHistory(Resource):
    # Claim history endpoint

    @cross_origin()
    @token_required
    @api_check_token_payload_match
    def post(self):
        try:
            user_data = request.get_json()
            user_name = user_data["user_name"]
            with connect_db() as cnx:
                # Create a cursor
                with cnx.cursor() as cursor:
                    # Get claim history
                    get_claim_history_query = "SELECT * FROM ClaimHistory WHERE user_name = %s"
                    values = (user_name,)
                    cursor.execute(get_claim_history_query, values)
                    result = cursor.fetchall()
                    # Commit the transaction
                    cnx.commit()
        except Error as e:
            logger.error(e)
            return {"code": ServerConst.MiscConst.general_server_side_error_code}, ServerConst.MiscConst.general_server_side_error_code

        result_list_of_dict = []
        for claimHistory in result:
            claim_dict = {
                'claim_id': claimHistory[0],
                'date': claimHistory[2].strftime("%Y-%m-%d %H:%M:%S"),
                'amount': float(claimHistory[3]),
                'type':  claimHistory[4],
                'status': claimHistory[5],
                'comment': claimHistory[6],
                'add_on': claimHistory[7]
            }
            result_list_of_dict.append(claim_dict)

        return {'claimHistory': result_list_of_dict}, ServerConst.MiscConst.general_success_code


# Add endpoints
api.add_resource(Login, '/auth/login')
api.add_resource(Logout, '/auth/logout')
api.add_resource(Register, '/register')
api.add_resource(User, '/user')
api.add_resource(Claim, '/claim')
api.add_resource(ClaimHistory, '/claimHistory')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
