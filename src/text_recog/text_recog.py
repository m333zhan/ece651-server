import cv2
import numpy as np
import base64
import pytesseract
import re
import logging
from scipy import ndimage
from src.const.server_const import TextRecogConst
from src.const import server_const as ServerConst

# Configure logger
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')
logger = logging.getLogger(__name__)


class TextRecog:
    def __init__(self, shaded=True, mode='l', thre=-1, sharpk=1):
        self.img = None
        self.gray = None
        self.img_binary = None

        if shaded:
            self.sharpness = TextRecogConst.Shaded_sharpness
        else:
            self.sharpness = TextRecogConst.Unshaded_sharpness

        if mode == 'l':
            self.threshold = TextRecogConst.Hough_thre_line
        else:
            self.threshold = TextRecogConst.Hough_thre_line_text

        if thre > 0:
            self.threshold = thre

        self.sharpk = sharpk
        self.min_rad = TextRecogConst.min_rad
        self.text = ''
        self.category = 'unclassified'
        self.amount = 0

    def open_image(self, img):
        try:
            if isinstance(img, str):
                if 'base64' in img:
                    _, img = img.split(",", 1)
                    decoded = base64.b64decode(img)
                    img_array = np.frombuffer(decoded, np.uint8)
                    self.img = cv2.imdecode(img_array, cv2.IMREAD_UNCHANGED)
                else:
                    self.path = img
                    self.img = cv2.imread(img)
            else:
                self.img = img
            self.gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
            _, image_data = cv2.imencode('.png', self.img)
            self.img_binary = image_data.tobytes()

        except Exception as e:
            logger.error(e.args)
            return ServerConst.MiscConst.general_client_side_error_code

        return ServerConst.MiscConst.general_success_code

    def text_recog(self, test=False):
        try:
            radian = self.hough()
            max_text, max_deg = self.best_recog(radian)

            self.text = max_text
            self.classify()
            self.recog_amount()

            if test:
                print(self.category, self.amount)

            if self.amount > 0:
                return ServerConst.MiscConst.general_success_code
            else:
                return ServerConst.MiscConst.claim_amount_unrecognized_error_code
        except Exception as e:
            logger.error(e.args)
            return ServerConst.MiscConst.claim_amount_unrecognized_error_code

    def classify(self):
        if re.search(r'(\s|^)[vV][iI][sS][iIuU]\S+', self.text):  # vision
            self.category = 'vision'
            return 'vision'

        if re.search(r'(\s|^)[eE][yY][eE](\s|\S+)', self.text):  # eye
            self.category = 'vision'
            return 'vision'

        if re.search(r'(\s|^)[sS][iI][gG][hH][tT](\s|$)', self.text):  # sight
            self.category = 'vision'
            return 'vision'

        if re.search(r'(\s|^)[dD][eE][nN][tT]\S+', self.text):  # dental
            self.category = 'dental'
            return 'dental'

        if re.search(r'[tT][oO][oO][tT][hH]', self.text):  # tooth
            self.category = 'dental'
            return 'dental'

        if re.search(r'[tT][eE][eE][tT][hH]', self.text):  # teeth
            self.category = 'dental'
            return 'dental'

        if re.search(r'\sDr\.\s\S+', self.text):  # Dr.
            self.category = 'health'
            return 'health'

        if re.search(r'(\s|^)[tT][rR][eE][aA][tT]\S*', self.text):  # treatment
            self.category = 'health'
            return 'health'

        if re.search(r'(\s|^)[mM][eE][dD][iI][cC]\S+', self.text):  # medicine
            self.category = 'health'
            return 'health'

        self.category = 'unrecognized'
        return 'unrecognized'

    def recog_amount(self):
        regex = re.compile(r'\$\s{0,1}(\d+,)*\d+\.{0,1}\d{0,2}')
        amounts = regex.finditer(self.text)
        for match in amounts:
            amount = match.group()
            amount_num = float(''.join(amount.strip('$').split(',')))
            self.amount = max(self.amount, amount_num)
        return self.amount

    # return the possible rotating angles, in radian
    def hough(self):
        edges = cv2.Canny(self.gray, TextRecogConst.T_lower,
                          TextRecogConst.T_upper)

        # all Hough Lines
        lines = cv2.HoughLines(edges, 1, np.pi / 180,
                               self.threshold, np.array([]))
        radian = []
        for line in lines:
            _, theta = line[0]
            radian.append(theta)

        # greedily delete angles too close
        radian.sort()
        diff_radian = []
        this = -20000
        for r in radian:
            if abs(r - this) > self.min_rad:
                diff_radian.append(r)
                this = r

        return diff_radian

    # return the best result of recognizing rotated image
    def best_recog(self, radian):
        max_text = pytesseract.image_to_string(self.img)
        max_deg = 0

        # use the line that can be best converted
        for theta in radian:
            deg = 180 * theta / np.pi - 90
            img_rotated = ndimage.rotate(self.gray, deg)
            s_i = self.sharpen(img_rotated, self.sharpness, ktype=self.sharpk)
            text = pytesseract.image_to_string(s_i)
            if len(text) > len(max_text):
                max_text = text
                max_deg = deg

        return max_text, max_deg

    # sharpen image for better recognition
    def sharpen(self, img, sharpness=50, ktype=1):
        n = sharpness / 100

        # two kinds of kernels
        if ktype == 1:
            sharpen_op = np.array([[0,   -n,   0],
                                   [-n, 4*n+1, -n],
                                   [0,   -n,   0]], dtype=np.float32)

        if ktype == 2:
            sharpen_op = np.array([[-n,  -n,   -n],
                                   [-n, 8*n+1, -n],
                                   [-n,  -n,   -n]], dtype=np.float32)

        img_sharpen = cv2.filter2D(img, cv2.CV_32F, sharpen_op)
        img_sharpen = cv2.convertScaleAbs(img_sharpen)

        return img_sharpen


if __name__ == '__main__':
    with open('./test_cases/Base64_1.txt', 'r') as file:
        text_rec_a = TextRecog(shaded=False)
        text_rec_a.open_image(file.read())
        print(text_rec_a.text_recog(test=True))
