import pytest

if __name__ == "__main__":
    test_names = ['test_api_user', 'test_api_register', 'test_text_recog',
                  'test_api_login', 'test_api_logout', 'test_api_claim',
                  'test_api_claim_history', 'test_general_func']
    pytest.main(test_names)
