from src.server import ServerConst
from unittest.mock import patch
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
import sys
import os
import shutil

test_header_json = {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0IiwiZXhwaXJlc19hdCI6MTY3OTc4NDU0MH0.4qifl7cP7zNI3JfVQcyEBjOOLzRmnnOMo2QLLnaZPWY",
    'Content-Type': 'multipart/form-data'
}

test_auth_header_json = {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0IiwiZXhwaXJlc19hdCI6MTY3OTc4NDU0MH0.4qifl7cP7zNI3JfVQcyEBjOOLzRmnnOMo2QLLnaZPWY"}

test_user_name = 'test'
test_type = 'unrecognized'
test_comment = 'test_comment'
test_add_on = 'test_add_on'
absolute_path_1 = os.path.join(
    os.getcwd(), 'src', 'text_recog', 'test_cases', 'Base64_1.txt')
with open(absolute_path_1, 'r') as test_pic_file:
    test_pic_base64_1 = test_pic_file.read()
absolute_path_neg = os.path.join(
    os.getcwd(), 'src', 'text_recog', 'test_cases', 'Base64_neg.txt')
with open(absolute_path_neg, 'r') as test_pic_file:
    test_pic_base64_neg = test_pic_file.read()


def test_claim_unsupported_content_type(client):
    # Test unsupported_content_type
    response = client.post('/claim', headers=test_auth_header_json,
                           json={"user_name": test_user_name,
                                 "type": test_type,
                                 "comment": test_comment,
                                 "add_on": test_add_on,
                                 "image": 'data:123'})
    assert response.status_code == 400
    assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                             "message": ServerConst.MiscConst.unsupported_content_type}


def test_claim_unauth(client):
    # Test unauth user
    response = client.post('/claim')
    assert response.status_code == 401
    assert response.json == {"error": ServerConst.MiscConst.client_side_auth_failure_error_code,
                             "message": ServerConst.MiscConst.authorization_failed}


def test_claim_post_missing_field_endpoint(client):
    # Test missing fields

    with patch('src.server.connect_db'):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post('/claim',
                               headers=test_header_json,
                               data={"user_name": test_user_name})

        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.ClaimAPIConst.missing_fields}
        ServerConst.MiscConst.expires_in = temp_exp


def test_claim_invalid_image_uploaded(client):
    # Test invalid image file uploaded

    with patch('src.server.connect_db'):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post('/claim',
                               headers=test_header_json,
                               data={"user_name": test_user_name,
                                     "type": test_type,
                                     "comment": test_comment,
                                     "add_on": test_add_on,
                                     "image": 'data:123'})

        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.ClaimAPIConst.invalid_image}
        ServerConst.MiscConst.expires_in = temp_exp


def test_claim_unrecognized(client):
    # Test unrecognized claim

    with patch('src.server.connect_db'):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post('/claim',
                               headers=test_header_json,
                               data={"user_name": test_user_name,
                                     "type": test_type,
                                     "comment": test_comment,
                                     "add_on": test_add_on,
                                     "image": test_pic_base64_neg})

        assert response.status_code == 402
        assert response.json == {"error": ServerConst.MiscConst.claim_amount_unrecognized_error_code,
                                 "message": ServerConst.ClaimAPIConst.amount_unrecognized}
        ServerConst.MiscConst.expires_in = temp_exp


def test_claim_no_id_generated(client):
    # Test no id generated

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        # For fetchone called more than once, use side_effect
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [1]
        return new_mock_mysql_connection

    # Mock MySQLConnection inside api since it is now out of api_check_token_payload_match
    def new_connect_db_inside_api():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = None
        return new_mock_mysql_connection

    # Use call count to record how many times the new_connect_db_wrapper is being called
    # Replace new_connect_db with new_connect_db_inside_api if the second time
    def new_connect_db_wrapper(*args, **kwargs):
        if not hasattr(new_connect_db_wrapper, "call_count"):
            new_connect_db_wrapper.call_count = 0
        new_connect_db_wrapper.call_count += 1
        if new_connect_db_wrapper.call_count == 2:
            return new_connect_db_inside_api(*args, **kwargs)
        return new_connect_db(*args, **kwargs)

    with patch('src.server.connect_db', new_connect_db_wrapper):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post('/claim',
                               headers=test_header_json,
                               data={"user_name": test_user_name,
                                     "type": test_type,
                                     "comment": test_comment,
                                     "add_on": test_add_on,
                                     "image": test_pic_base64_1})

        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.ClaimAPIConst.insertion_failed}
        ServerConst.MiscConst.expires_in = temp_exp


def test_claim_success(client):
    # Test successful claim

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [15000]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post('/claim',
                               headers=test_header_json,
                               data={"user_name": test_user_name,
                                     "type": test_type,
                                     "comment": test_comment,
                                     "add_on": test_add_on,
                                     "image": test_pic_base64_1})

        relative_path = "./claimHistory"

        # Delete the directory and its contents
        shutil.rmtree(relative_path)

        assert response.status_code == 200
        ServerConst.MiscConst.expires_in = temp_exp
