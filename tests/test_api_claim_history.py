from src.server import ServerConst
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
from decimal import Decimal
import pytest
import sys
import datetime

test_auth_header_json = {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0IiwiZXhwaXJlc19hdCI6MTY3OTc4NDU0MH0.4qifl7cP7zNI3JfVQcyEBjOOLzRmnnOMo2QLLnaZPWY"}

test_db_claim_history_return = [(1, 'test', datetime.datetime(2023, 3, 25, 22, 16, 51),
                                 Decimal('350.22'), 'Dental', 'Processing', 'Test comment 1', 'Test Add on 1'),
                                (2, 'test', datetime.datetime(2023, 3, 25, 22, 16, 51),
                                 Decimal('5310.30'), 'Massage', 'Finished', 'Test comment 2', 'Test Add on 2')]


def test_claim_history_post_success(client):
    # Test api success

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        # For fetchone called more than once, use side_effect
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [1]
        return new_mock_mysql_connection

    # Mock MySQLConnection inside api since it is now out of api_check_token_payload_match
    def new_connect_db_inside_api():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchall.return_value = test_db_claim_history_return
        return new_mock_mysql_connection

    # Use call count to record how many times the new_connect_db_wrapper is being called
    # Replace new_connect_db with new_connect_db_inside_api if the second time
    def new_connect_db_wrapper(*args, **kwargs):
        if not hasattr(new_connect_db_wrapper, "call_count"):
            new_connect_db_wrapper.call_count = 0
        new_connect_db_wrapper.call_count += 1
        if new_connect_db_wrapper.call_count == 2:
            return new_connect_db_inside_api(*args, **kwargs)
        return new_connect_db(*args, **kwargs)

    with patch('src.server.connect_db', new_connect_db_wrapper):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post(
            '/claimHistory', headers=test_auth_header_json, json={"user_name": "test"})
        assert response.status_code == 200
        assert response.json == {'claimHistory': [
            {
                'claim_id': test_db_claim_history_return[0][0],
                'date': test_db_claim_history_return[0][2].strftime("%Y-%m-%d %H:%M:%S"),
                'amount': float(test_db_claim_history_return[0][3]),
                'type':  test_db_claim_history_return[0][4],
                'status': test_db_claim_history_return[0][5],
                'comment': test_db_claim_history_return[0][6],
                'add_on': test_db_claim_history_return[0][7]
            },
            {
                'claim_id': test_db_claim_history_return[1][0],
                'date': test_db_claim_history_return[1][2].strftime("%Y-%m-%d %H:%M:%S"),
                'amount': float(test_db_claim_history_return[1][3]),
                'type':  test_db_claim_history_return[1][4],
                'status': test_db_claim_history_return[1][5],
                'comment': test_db_claim_history_return[1][6],
                'add_on': test_db_claim_history_return[1][7]
            }]}
        ServerConst.MiscConst.expires_in = temp_exp
