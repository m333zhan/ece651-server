from src.server import ServerConst
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
import pytest

new_user_name = "jjj"


def test_login_endpoint(client):
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = ['test']
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/auth/login', json={"user_name": "test", "password": "test"})
        assert response.status_code == 200


def test_login_endpoint_missing_fields(client):
    response = client.post(
        '/auth/login', json={"user_name": new_user_name})
    assert response.status_code == 400
    assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                             "message": ServerConst.AuthAPIConst.missing_fields}


def test_login_endpoint_wrong_pwd(client):
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = ['wrong']
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/auth/login', json={"user_name": "test", "password": "test"})
        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.AuthAPIConst.wrong_user_name_or_password}
