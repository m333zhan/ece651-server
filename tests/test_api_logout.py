from src.server import ServerConst
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
import pytest

test_auth_header_json = {
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0IiwiZXhwaXJlc19hdCI6MTY3OTI4NjMwMH0.60KcJsqG1JtELCSGwbgwtVK5r9ymAQMHnl_jr0DI-BM"}


def test_logout_auth(client):
    response = client.post('/auth/logout')
    assert response.status_code == 401
    assert response.json == {"error": ServerConst.MiscConst.client_side_auth_failure_error_code,
                             "message": ServerConst.MiscConst.authorization_failed}


def test_logout_endpoint(client):
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/auth/logout', headers=test_auth_header_json)
        assert response.status_code == 200
