from src.server import ServerConst
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
import pytest

new_user_name = "jjj"
new_user_password = "1323"
new_user_email = "sss@mmm.com"


def test_register_endpoint_missing_fields(client):
    # Test missing fields
    response = client.post(
        '/register', json={"user_name": new_user_name, "password": new_user_password})
    assert response.status_code == 400
    assert response.json == {
        "error": 400, "message": ServerConst.RegisterAPIConst.missing_fields}


def test_register_endpoint_user_exist(client):
    # Test user exist

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [1]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/register', json={"user_name": "test", "password": "test", "email": "test@test.com"})
        assert response.status_code == 400
        assert response.json == {"error": 400,
                                 "message": ServerConst.RegisterAPIConst.user_exist}


def test_register_endpoint_user_creation(client):
    # Test user creation

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [0]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/register', json={"user_name": new_user_name, "password": new_user_password, "email": new_user_email})
        assert response.status_code == 200
