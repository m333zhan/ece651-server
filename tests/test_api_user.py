from src.server import ServerConst
from mysql.connector import MySQLConnection
from unittest.mock import patch, MagicMock
import pytest
import sys

test_auth_header_json = {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJ0ZXN0IiwiZXhwaXJlc19hdCI6MTY3OTc4NDU0MH0.4qifl7cP7zNI3JfVQcyEBjOOLzRmnnOMo2QLLnaZPWY"}

test_user_name = 'test'
test_email = 'test@email'
test_student_id = '55555555555'
test_first_name = "TestTT"
test_last_name = "TTEESSTT"
test_sex = "TSEX"
test_birthday = "20180918"


def test_user_auth(client):
    response = client.post('/user')
    assert response.status_code == 401
    assert response.json == {"error": ServerConst.MiscConst.client_side_auth_failure_error_code,
                             "message": ServerConst.MiscConst.authorization_failed}


def test_user_post_missing_field_endpoint(client):
    # Test missing fields
    response = client.post('/user', headers=test_auth_header_json, json={})
    assert response.status_code == 400
    assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                             "message": ServerConst.RegisterAPIConst.missing_fields}


def test_user_post_decoded_user_name_unmatch_endpoint(client):
    # Test missing fields
    response = client.post(
        '/user', headers=test_auth_header_json, json={"user_name": 'lll'})
    assert response.status_code == 400
    assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                             "message": ServerConst.UserAPIConst.unauthorized_access}


def test_user_post_user_does_not_exist(client):
    # Test user does not exist

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [0]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/user', headers=test_auth_header_json, json={"user_name": "test"})
        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.UserAPIConst.user_does_not_exist}


def test_user_post_token_does_not_exists(client):
    # Test token does not exist

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        # For fetchone called more than once, use side_effect
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.side_effect = [[1] if i == 0 else [0] for i in range(2)]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/user', headers=test_auth_header_json, json={"user_name": "test"})
        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.UserAPIConst.token_expired}


def test_user_post_token_expire(client):
    # Test token expires

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        # For fetchone called more than once, use side_effect
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [1]
        return new_mock_mysql_connection

    with patch('src.server.connect_db', new_connect_db):
        response = client.post(
            '/user', headers=test_auth_header_json, json={"user_name": "test"})
        assert response.status_code == 400
        assert response.json == {"error": ServerConst.MiscConst.general_client_side_error_code,
                                 "message": ServerConst.UserAPIConst.unauthorized_access}


def test_user_post_success(client):
    # Test api success

    # Mock MySQLConnection
    def new_connect_db():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        # For fetchone called more than once, use side_effect
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [1]
        return new_mock_mysql_connection

    # Mock MySQLConnection inside api since it is now out of api_check_token_payload_match
    def new_connect_db_inside_api():
        new_mock_mysql_connection = MagicMock(
            spec=MySQLConnection, return_value='patched')
        new_mock_mysql_connection.__enter__().cursor(
        ).__enter__().fetchone.return_value = [test_user_name, '',
                                               test_email,
                                               test_student_id,
                                               test_first_name,
                                               test_last_name,
                                               test_sex,
                                               test_birthday]
        return new_mock_mysql_connection

    # Use call count to record how many times the new_connect_db_wrapper is being called
    # Replace new_connect_db with new_connect_db_inside_api if the second time
    def new_connect_db_wrapper(*args, **kwargs):
        if not hasattr(new_connect_db_wrapper, "call_count"):
            new_connect_db_wrapper.call_count = 0
        new_connect_db_wrapper.call_count += 1
        if new_connect_db_wrapper.call_count == 2:
            return new_connect_db_inside_api(*args, **kwargs)
        return new_connect_db(*args, **kwargs)

    with patch('src.server.connect_db', new_connect_db_wrapper):
        temp_exp = ServerConst.MiscConst.expires_in
        ServerConst.MiscConst.expires_in = sys.maxsize
        response = client.post(
            '/user', headers=test_auth_header_json, json={"user_name": "test"})
        assert response.status_code == 200
        assert response.json == {'user_name': test_user_name,
                                 'email': test_email,
                                 'student_id': test_student_id,
                                 'first_name': test_first_name,
                                 'last_name': test_last_name,
                                 'sex': test_sex,
                                 'birthday': test_birthday}
        ServerConst.MiscConst.expires_in = temp_exp
