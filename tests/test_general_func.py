from src.server import ServerConst, generate_token, decode_token

new_user_name = "jjj"


def test_generate_decode_token():
    token, exp = generate_token(new_user_name)
    decoded_payload = decode_token(token)
    assert decoded_payload["user_name"] == new_user_name
    assert int(exp.timestamp()) == decoded_payload["expires_at"]
