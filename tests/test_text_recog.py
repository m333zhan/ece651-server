from src.const import server_const as ServerConst
from src.text_recog.text_recog import TextRecog
import os


def test_rotated_images():
    absolute_path = os.path.join(
        os.getcwd(), 'src', 'text_recog', 'test_cases', 'Base64_1.txt')
    with open(absolute_path, 'r') as file:
        image = file.read()
    recognizer = TextRecog()
    open_result = recognizer.open_image(image)
    recog_result = recognizer.text_recog()
    category = recognizer.category
    amount = recognizer.amount

    assert open_result == ServerConst.MiscConst.general_success_code
    assert recog_result == ServerConst.MiscConst.general_success_code
    assert category == 'unrecognized'
    assert amount == 1234.57


def test_shot_images():
    absolute_path = os.path.join(
        os.getcwd(), 'src', 'text_recog', 'test_cases', 'bill2.jpg')
    recognizer = TextRecog()
    open_result = recognizer.open_image(absolute_path)
    recog_result = recognizer.text_recog()
    category = recognizer.category
    amount = recognizer.amount

    assert open_result == ServerConst.MiscConst.general_success_code
    assert recog_result == ServerConst.MiscConst.general_success_code
    assert category == 'unrecognized'
    assert amount == 2385.39


def test_unrecognized():
    absolute_path = os.path.join(
        os.getcwd(), 'src', 'text_recog', 'test_cases', 'Base64_neg.txt')
    with open(absolute_path, 'r') as file:
        image = file.read()
    recognizer = TextRecog()
    open_result = recognizer.open_image(image)
    recog_result = recognizer.text_recog()

    assert open_result == ServerConst.MiscConst.general_success_code
    assert recog_result == ServerConst.MiscConst.claim_amount_unrecognized_error_code

def test_invalid_image():
    recognizer = TextRecog()
    open_result = recognizer.open_image('data:123')

    assert open_result == ServerConst.MiscConst.general_client_side_error_code
